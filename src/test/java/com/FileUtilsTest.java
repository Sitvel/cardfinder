package com;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilsTest {

    private static final String FILE_CREATE = "fileCreate";
    private static final String FILE_DELETE = "fileDelete";
    private static final String FILE_RECREATE = "fileRecreate";
    private static final String FILE_VALIDATE = "fileValidate";
    private static final String FILE_WRITE = "fileWrite";

    @BeforeAll
    static void beforeAll() {
        try {
            new File(FILE_DELETE).createNewFile();
            Files.write(Paths.get(FILE_RECREATE), FILE_RECREATE.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    static void afterAll() {
        new File(FILE_CREATE).deleteOnExit();
        new File(FILE_DELETE).deleteOnExit();
        new File(FILE_VALIDATE).deleteOnExit();
        new File(FILE_RECREATE).deleteOnExit();
        new File(FILE_WRITE).deleteOnExit();
    }

    @Test
    void fileCreate() throws IOException {
        File file = new File(FILE_CREATE);
        assertEquals(false, file.exists());

        FileUtils.UTILS.fileCreate(Paths.get(FILE_CREATE));
        assertEquals(true, file.exists());
    }


    @Test
    void validateInput() throws IOException {
        File file = new File(FILE_VALIDATE);
        assertFalse(FileUtils.UTILS.validateInput(file));

        file.createNewFile();
        assertFalse(FileUtils.UTILS.validateInput(file));

        file.delete();
        file.mkdir();
        assertTrue(FileUtils.UTILS.validateInput(file));

    }

    @Test
    void fileRecreate() throws IOException {
        File file = new File(FILE_RECREATE);
        long length1 = file.length();

        FileUtils.UTILS.fileRecreate(Paths.get(FILE_RECREATE));
        long length2 = file.length();

        assertFalse(length1 == length2);
    }

    @Test
    void fileDelete() {
        File file = new File(FILE_DELETE);
        assertEquals(true, file.exists());

        FileUtils.UTILS.fileDelete(Paths.get(FILE_DELETE));
        assertFalse(file.exists());
    }


    @Test
    void writeToFile() throws IOException {
        File file = new File(FILE_WRITE);
        assertFalse(file.exists());

        file.createNewFile();
        int len1 = Files.readAllLines(Paths.get(FILE_WRITE)).size();
        FileUtils.UTILS.writeToFile(Arrays.asList("test1", "test2"), Paths.get(FILE_WRITE));
        int len2 = Files.readAllLines(Paths.get(FILE_WRITE)).size();
        assertFalse(len1 == len2);
    }
}
