package com;

import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FileReaderProducerTest {

    @Test
    void validateLines() {
        FileReaderProducer frp = new FileReaderProducer(Paths.get("frpFrom"), Paths.get("frpTo"));

        assertEquals(0, frp.validateLines(null).size());

        assertEquals(0, frp.validateLines(Collections.singletonList("Qqq,www,1212,rt")).size());

        List<String> listOneCard = Arrays.asList(
            "Qqq,www,1212,rt",
            "Aaa,bbb,5457623898234113,ccc,ddd",
            "Fff,ggg,1234567890123456,ttt,yyy");
        assertEquals(1, frp.validateLines(listOneCard).size());
    }
}
