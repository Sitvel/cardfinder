package com;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.*;

class CardFinder {
    private static final String SOURCE = "source";
    private static final String TARGET = "target";
    private static final int poolSize = Runtime.getRuntime().availableProcessors();
    private static final String INTERNAL_ERROR_PLEASE_CONTACT_A_SUPPORT_TEAM = "Internal error. Please contact a support team";
    private static final String YOU_LL_FIND_A_FILE_IN_A_TARGET_DIRECTORY_THANK_YOU_FOR_USING_OUR_SERVICE = "You'll find a file in a target directory. Thank you for using our service.";
    private static final String PLEASE_WAIT = "Please wait...";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        File[] sourceDirFiles = FileUtils.UTILS.readInput(SOURCE, scanner).listFiles((dir, name) -> name.toLowerCase().endsWith(".csv"));
        File targetDir = FileUtils.UTILS.readInput(TARGET, scanner);
        scanner.close();

        processFolder(sourceDirFiles, targetDir);
        System.out.println(YOU_LL_FIND_A_FILE_IN_A_TARGET_DIRECTORY_THANK_YOU_FOR_USING_OUR_SERVICE);
    }

    private static void processFolder(File[] sourceDirFiles, File targetDir) {
        final ExecutorService pool = Executors.newFixedThreadPool(poolSize);
        final CompletionService<String> service = new ExecutorCompletionService<>(pool);

        System.out.println(PLEASE_WAIT);

        Arrays.stream(sourceDirFiles)
                .map(file -> new FileReaderProducer(file.toPath(), targetDir.toPath()))
                .forEach(service::submit);

        pool.shutdown();

        try {
            while (!pool.isTerminated()) {
                System.out.println(service.take().get());
            }
        } catch (ExecutionException | InterruptedException ex) {
            System.out.println(INTERNAL_ERROR_PLEASE_CONTACT_A_SUPPORT_TEAM);
        }
    }
}