package com;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.validator.routines.checkdigit.LuhnCheckDigit;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import static java.nio.file.Files.newBufferedReader;
import static java.util.stream.Collectors.toList;

class FileReaderProducer implements Callable<String> {
    private final Path path;
    private final Path targetFilePath;
    private final int BATCH_SIZE = 10000;

    public FileReaderProducer(Path path, Path targetPath) {
        this.path = path;
        this.targetFilePath = Paths.get(targetPath.toString() + File.separator + path.getFileName());
    }

    @Override
    public String call() {
        System.out.println(targetFilePath + " started to process");

        if (path == null || targetFilePath == null) {
            return "Path and targetFilePath shouldn't be null";
        }

        try {
            FileUtils.UTILS.fileRecreate(targetFilePath);
        } catch (IOException e) {
            return returnException();
        }

        try (BufferedReader reader = newBufferedReader(path)) {
            readLineByLine(reader);
        } catch (IOException e1) {
            return returnException();
        }

        return "Processing " + path + " finished.";
    }

    private String returnException() {
        return ("Error while processing a file " + path);
    }

    //we have file up to 200mb so potentially we can read all file in one shot by Files.readAllLines(path) and validate it,
    // but in case we have bigger file we can use a safe approach
    private void readLineByLine(BufferedReader reader) throws IOException {
        List<String> result = new LinkedList<>();
        for (; ; ) {
            String line = reader.readLine();

            if (line == null) {
                FileUtils.UTILS.writeToFile(validateLines(result), targetFilePath);
                return;
            }

            result.add(line);

            if (result.size() == BATCH_SIZE) {
                FileUtils.UTILS.writeToFile(validateLines(result), targetFilePath);
                result.clear();
            }
        }
    }

    List<String> validateLines(List<String> subList) {
        return CollectionUtils.isEmpty(subList) ? Collections.emptyList()
            : subList.parallelStream()
            .filter(line -> Arrays.stream(line.split(",")).anyMatch(LuhnCheckDigit.LUHN_CHECK_DIGIT::isValid))
            .collect(toList());
    }
}
