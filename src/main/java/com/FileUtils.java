package com;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Scanner;

public enum FileUtils {

    UTILS;

    private static final String ENTER_A_6S_DIRECTORY_PATH = "Enter a %6s directory path: ";
    private static final String CAN_T_READ_FROM_A_SOURCE_DIRECTORY_PLEASE_ENTER_A_VALID_PATH_TO_DIRECTORY = "Can't read from a source directory. Please enter a valid path to directory.";

    public File readInput(String dirName, Scanner scanner) {
        while (true) {
            System.out.printf(ENTER_A_6S_DIRECTORY_PATH, dirName);
            File dir = new File(scanner.nextLine());
            if (FileUtils.UTILS.validateInput(dir)) {
                return dir;
            } else {
                System.out.println(CAN_T_READ_FROM_A_SOURCE_DIRECTORY_PLEASE_ENTER_A_VALID_PATH_TO_DIRECTORY);
            }
        }
    }

    public boolean validateInput(File dir) {
        return dir.exists() && dir.isDirectory();
    }

    public void fileRecreate(Path targetFilePath) throws IOException {
        fileDelete(targetFilePath);
        fileCreate(targetFilePath);
    }

    public void fileDelete(Path targetFilePath) {
        File file = targetFilePath.toFile();
        if (file.exists()) {
            file.delete();
        }
    }

    public void fileCreate(Path targetFilePath) throws IOException {
        File file = targetFilePath.toFile();
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    public void writeToFile(List<String> list, Path targetFilePath) throws IOException {
        Files.write(targetFilePath, list, StandardOpenOption.APPEND);
    }
}
